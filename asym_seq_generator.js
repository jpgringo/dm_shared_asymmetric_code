autowatch = 1;

this.outlets = 2;

var MAIN_SEQ_NAME = "mSeq";

var main_seq = this.patcher.getnamed('main_seq');
post("main_seq=" + main_seq);

function test_input(input) {
    post("input: " + input + " [" + (typeof input) + "]\n");
}

function create_basic_seq(totalPulses) {
    post("will create basic seq");
    main_seq.seq(MAIN_SEQ_NAME);
    main_seq.clear('all');
    main_seq.add(MAIN_SEQ_NAME, 0.0, 'cycle');
    main_seq.add(MAIN_SEQ_NAME, 0.0, 'div');
    for(var i=0; i < totalPulses; i++)
    {
        post(i);
        main_seq.add(MAIN_SEQ_NAME, i * (1.0/totalPulses), 'pulse');
    }
    main_seq.play(1);
    main_seq.dump();
}

function create_canonic_reversal_pattern(totalPulses, splitPoint) {
    if(splitPoint == 0) {
        create_basic_seq(totalPulses);
        return;
    }
    post("will create canonic reversal pattern based on " + totalPulses + " total pulses, split at " + splitPoint + '\n');
    var pulseGroups = [totalPulses - splitPoint, splitPoint];
    var groupStarts = [0.0, splitPoint / totalPulses];
    post("pulse groups:" + pulseGroups + "\n");
    post("group starts:" + groupStarts + "\n");

    // build the sequence
    main_seq.seq(MAIN_SEQ_NAME);
    main_seq.clear('all');
    main_seq.add(MAIN_SEQ_NAME, 0.0, 'cycle');
    var groupLen, groupPulseWidth;
    for(var i=0; i < pulseGroups.length; i++) {
        main_seq.add(MAIN_SEQ_NAME, groupStarts[i], 'div');
        groupLen  = (i == pulseGroups.length - 1 ? 1.0 : groupStarts[i+1])
            - (i == 0 ? 0.0 : groupStarts[i]);
        groupPulseWidth = groupLen / pulseGroups[i];
        post("group [" + i + "]: len=" + groupLen + ';width=' + groupPulseWidth + '\n');
        for(var j=0; j < pulseGroups[i]; j++) {
            main_seq.add(MAIN_SEQ_NAME, groupStarts[i] + j*groupPulseWidth, 'pulse');
        }
    }
    // build the click
    var clickWidth = 1.0 / totalPulses;
    for(var i=0; i < totalPulses; i++) {
        main_seq.add(MAIN_SEQ_NAME,i*clickWidth, 'click');
    }
    main_seq.play(1);
    main_seq.dump();

}